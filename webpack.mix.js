let mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        alias: { picker: 'pickadate/lib/picker' }
    }
});

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery']
});

mix.js('assets/js/cpt.js', 'assets/')
   .sass('assets/sass/cpt.scss', 'assets/')
   .sourceMaps();
