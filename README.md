# Ayctor Custom Post Type

## Introduction

The goal of this package is to facilitate the making of Custom Post Type (CPT) within wordpress.

## Get Started

First install it with composer

```
composer require ayctor/cpt
```

Create a Class for the New CPT

```
use CustomPostType\Model;

class Example extends Model
{
}
```

Declare your new class in function.php

```
new CustomPostType\Admin; // required for assets registration
new Example;
```

## Example of fields

```
// name of your post type
protected $internal_name = 'example';

// generate labels, you can also use protected $labels
protected $name = 'Examples';

// list of args for register_post_type()
protected $cpt_args = [
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon' => 'dashicons-carrot',
    'rewrite' => true,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'excerpt' )
];

// list of args for register_taxonomy()
protected $taxonomies = [
    'carrot_cat' => [
        'label' => 'Country',
        'hierarchical' => true,
        'rewrite' => ['slug' => '/country']
    ],
    'carrot_tag' => [
        'label' => 'Colors',
        'hierarchical' => false,
        'rewrite' => ['slug' => '/colors']
    ]
];


// Load all data to CPT editor page
public function adminEditor()
{
     // Add meta box
    $this->metaBox('ay-carrot-meta', 'Carrots');

    // Add fields
    $this->label('Information', 'carrot_info', 'ay-carrot-meta');
    $this->info('carrot_info', 'ay-carrot-meta');

    $this->label('Text', 'carrot_text', 'ay-carrot-meta');
    $this->text('carrot_text', 'ay-carrot-meta');

    $this->label('Text multiple', 'carrot_text_multiple', 'ay-carrot-meta');
    $this->text('carrot_text_multiple', 'ay-carrot-meta', ['multiple' => true]);

    $this->label('Text Disabled', 'carrot_disabled', 'ay-carrot-meta');
    $this->text('carrot_disabled', 'ay-carrot-meta', ['attributes' => ['disabled' => 'disabled']]);

    $this->label('Number', 'carrot_number', 'ay-carrot-meta');
    $this->number('carrot_number', 'ay-carrot-meta');

    $this->label('Date', 'carrot_date', 'ay-carrot-meta');
    $this->date('carrot_date', 'ay-carrot-meta');

    $this->label('Time', 'carrot_time', 'ay-carrot-meta');
    $this->time('carrot_time', 'ay-carrot-meta');

    $this->label('File', 'carrot_file', 'ay-carrot-meta');
    $this->file('carrot_file', 'ay-carrot-meta');

    $this->label('File multiple', 'carrot_file_multiple', 'ay-carrot-meta');
    $this->file('carrot_file_multiple', 'ay-carrot-meta', ['multiple' => true]);

    $this->label('Textarea', 'carrot_textarea', 'ay-carrot-meta');
    $this->textarea('carrot_textarea', 'ay-carrot-meta');

    $this->label('Textarea disabled', 'carrot_textarea_disabled', 'ay-carrot-meta');
    $this->textarea('carrot_textarea_disabled', 'ay-carrot-meta', ['attributes' => ['disabled' => 'disabled']]);

    $this->label('Select', 'carrot_select', 'ay-carrot-meta');
    $this->select('carrot_select', 'ay-carrot-meta', ['options' => ['are' => 'yes', 'eest' => 'testons']]);

    $this->label('Select multiple', 'carrot_select_multiple', 'ay-carrot-meta');
    $this->select('carrot_select_multiple', 'ay-carrot-meta', ['multiple' => true, 'options' => ['are' => 'yes', 'test' => 'no']]);

    $this->label('Radio', 'carrot_radio', 'ay-carrot-meta');
    $this->radio('carrot_radio', 'ay-carrot-meta', ['options' => ['areyou' => 'yes', 'ares' => 'se y'], 'inline' => false]);

    $this->label('Checkbox', 'carrot_checkbox', 'ay-carrot-meta');
    $this->checkbox('carrot_checkbox', 'ay-carrot-meta', ['options' => ['areyou' => 'yes', 'ares' => 'se y']]);

    $this->editor('carrot_editor', 'ay-carrot-meta');

    $this->label('Image', 'carrot_image', 'ay-carrot-meta');
    $this->image('carrot_image', 'ay-carrot-meta');

    $this->label('Gallery', 'carrot_gallery', 'ay-carrot-meta');
    $this->image('carrot_gallery', 'ay-carrot-meta', ['gallery' => true]);

    $this->label('CPT', 'carrot_cpt', 'ay-carrot-meta');
    $this->cpt('carrot_cpt', 'ay-carrot-meta', ['args' => ['post_type' => 'image']]);

    $this->label('CPT Multiple', 'carrot_cpt_multiple', 'ay-carrot-meta');
    $this->cpt('carrot_cpt_multiple', 'ay-carrot-meta', ['multiple' => true]);

    // Custom columns
    $this->column('cb', '<input type="checkbox" />', true);
    $this->column('title', 'Title', true);
    $this->column('carrot_thumb', 'Thumbnail', false, 'thumbnail');
    $this->column('carrot_time', 'Time', true, 'meta');
    $this->column('carrot_cat', 'Country', false, 'term');
    $this->column('carrot_tag', 'Colors', false, 'term');
    $this->column('carrot_custom', 'Custom', false, 'custom', 'My custom value');
    $this->column('date', 'Date', true);

    // Custom filters
    $this->filter('carrot_radio', array( '' => 'Carrot radio test', 'yes' => 'Yes', 'no' => 'No' ));
    $this->filter('test', array( '' => 'test', 'yes' => 'Yes', 'no' => 'No'));
}
```

## Usefuls links

List of args for register_post_type : https://codex.wordpress.org/Function_Reference/register_post_type

List of args for register_taxonomy : https://codex.wordpress.org/Function_Reference/register_taxonomy
