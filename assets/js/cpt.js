'use strict';

window.jQuery = window.$ = require('jquery');
require('jquery-ui/ui/widgets/sortable');
require('jquery-ui/ui/disable-selection');

require('select2');
require('select2/dist/js/i18n/fr.js');

require('pickadate/lib/picker');
require('pickadate/lib/picker.date');
require('pickadate/lib/picker.time');

$(document).ready(function () {
    // Init select 2
    $('.form-group select').select2({
        placeholder: 'Sélectionner',
        allowClear: true
    });

    require('./includes/datepicker');
    require('./includes/timepicker');
    require('./includes/files');
    require('./includes/filesMultiple');
    require('./includes/images');
    require('./includes/gallery');
    require('./includes/selectMultiple');
    require('./includes/linesMultiple');
});
