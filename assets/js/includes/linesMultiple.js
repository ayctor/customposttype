// Add line
$(document.body).on('click', '.aycpt-addline', function(e){
    e.preventDefault();
    let container = $(this).parents('.text-multiple-container');
    let parent = $(this).parent();
    let clone = parent.clone();

    parent.find('.aycpt-addline').addClass('hide');

    clone.find('input').val('');
    clone.find('.aycpt-deleteline').removeClass('hide');

    container.append(clone);
});

// Delete line
$(document.body).on('click', '.aycpt-deleteline', function(e){
    e.preventDefault();
    let container = $(this).parents('.text-multiple-container');
    $(this).parent().remove();
    container.find('.aycpt-addline').last().removeClass('hide');
});
