// Init sortable
if($('.galeries').length > 0){
    $('.galeries .sortable').sortable();
    $('.galeries .sortable').disableSelection();
}

// Add visuel
$(document.body).on('click', '.galeries .aycpt-addgallery', function (e) {
    e.preventDefault();

    let ayMediaframe;
    const container = $(this).parent().find('.sortable');
    const target = $(this).data('target');

    if (undefined !== ayMediaframe) {
        ayMediaframe.open();
        return;
    }

    ayMediaframe = wp.media.frames.ayMediaframe = wp.media({
        frame: 'post',
        multiple: true,
    });

    ayMediaframe.on( 'open', function() {
        $('.media-frame-title h1').text('Ajouter une image : ');
        $('.media-button-insert').text('Ajouter une image ');
    });

    $('.media-button-insert').on('change', function(){
        $(this).text('Ajouter une image ');
    });

    ayMediaframe.on('insert', function () {
        const selection = ayMediaframe.state().get('selection');
        selection.each(function(element){
            const mediaAttachment = element.toJSON();
            let url = urls.templateUrl + '/vendor/ayctor/cpt/assets/images/interface.png';
            if(mediaAttachment.url != undefined) url = mediaAttachment.url;
            const clone = container.find('.model').clone();
            clone.find('input').attr('name', `${target}[]`);
            clone.find('input').val(mediaAttachment.id);
            clone.find('img').attr('src', url);
            clone.removeClass('model hide');
            clone.appendTo(container);
        });
    });

    ayMediaframe.open();
});

// Delete visuel
$(document.body).on('click', '.galeries .dashicons-no-alt', function () {
    $(this).parent().remove();
});
