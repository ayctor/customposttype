// Select value
$(document.body).on('click', '.multiple-select-add', function(e){
    e.preventDefault();
    const parent = $(this).parent();
    const select = parent.find('select');
    const val = select.val();
    if (val !== '' && val !== null) {
        const selected = select.find('option:selected');
        const text = selected.text();
        const clone = parent.find('.model').clone();
        clone.find('input').val(val);
        clone.find('span.text').text(text);
        clone.removeClass('model hide');
        clone.appendTo(parent.find('ul'));

        selected.attr('disabled', 'disabled');
        const selectOptions = select.data('select2').options.options;
        select.val('').trigger('change');
        select.select2(selectOptions);
    }
});

// Delete value
$(document.body).on('click', '.multiple-select-delete', function(e){
    e.preventDefault();
    const val = $(this).parent().find('input').val();
    const select = $(this).parents('.multiple-select').find('select');
    select.find(`option[value="${val}"]`).removeAttr('disabled');
    const selectOptions = select.data('select2').options.options;
    select.select2(selectOptions);
    $(this).parent().remove();
});
