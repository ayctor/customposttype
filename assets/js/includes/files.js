// Add File
$(document.body).on('click', '.aycpt-addfile', function (e) {
    e.preventDefault();

    let ayMediaframe;
    const target = $(this).data('target');
    const label = $(this).data('label');

    if (undefined !== ayMediaframe) {
        ayMediaframe.open();
        return;
    }

    ayMediaframe = wp.media.frames.ayMediaframe = wp.media({
        frame: 'post',
        multiple: false,
    });

    ayMediaframe.on( 'open', function() {
        $('.media-frame-title h1').text('Ajouter ' + label);
        $('.media-button-insert').text('Sélectionner comme ' + label);
    });

    ayMediaframe.on('insert', function () {
        const mediaAttachment = ayMediaframe.state().get('selection').first().toJSON();
        const parent = $('#' + target).parent();
        $('#' + target).val(mediaAttachment.id);
        parent.find('.filename a').attr('href', mediaAttachment.url);
        parent.find('.filename a').text(mediaAttachment.filename);
        parent.find('.filename a').removeClass('hide');
        parent.find('.aycpt-addfile').addClass('hide');
        parent.find('.aycpt-modifyfile').removeClass('hide');
        parent.find('.aycpt-deletefile').removeClass('hide');
    });

    ayMediaframe.open();
});

// Delete File
$(document.body).on('click', '.aycpt-deletefile', function (e) {
    e.preventDefault();
    const target = $(this).data('target');
    const parent = $('#' + target).parent();
    $('#' + target).val('');
    parent.find('.filename a').addClass('hide');
    parent.find('.aycpt-addfile').toggleClass('hide');
    parent.find('.aycpt-deletefile').addClass('hide');
});
