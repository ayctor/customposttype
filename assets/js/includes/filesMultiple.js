// Add line
$(document.body).on('click', '.aycpt-addlinemulti', function (e) {
    e.preventDefault();
    const target = $(this).data('target');
    let container = $('#' + target);
    let parent = $(this).parent();
    let clone = parent.clone();

    parent.find('.aycpt-addlinemulti').addClass('hide');

    clone.find('input').val('');
    clone.find('.file-action').addClass('hide');
    clone.find('.file-action .aycpt-showfilemulti').attr('href', '');
    clone.find('.aycpt-addfilemulti').removeClass('hide');
    clone.find('.aycpt-deletelinemulti').removeClass('hide');

    container.append(clone);
});

// Delete line
$(document.body).on('click', '.aycpt-deletelinemulti', function (e) {
    e.preventDefault();
    const target = $(this).data('target');
    let container = $('#' + target);
    $(this).parent().remove();
    container.find('.aycpt-addlinemulti').last().removeClass('hide');
});

// Add file
$(document.body).on('click', '.aycpt-addfilemulti', function (e) {
    e.preventDefault();

    let ayMediaframe;
    const label = $(this).data('label');
    let $this = $(this);
    if (undefined !== ayMediaframe) {
        ayMediaframe.open();
        return;
    }

    ayMediaframe = wp.media.frames.ayMediaframe = wp.media({
        frame: 'post',
        multiple: true,
    });

    ayMediaframe.on( 'open', function() {
        $('.media-frame-title h1').text('Ajouter ' + label);
        $('.media-button-insert').text('Sélectionner comme ' + label);
    });

    $('.media-button-insert').on('change', function(){
        $(this).text('Sélectionner comme ' + label);
    });

    ayMediaframe.on('insert', function () {
        const selection = ayMediaframe.state().get('selection');
        selection.each(function(element){
            const mediaAttachment = element.toJSON();
            const parent = $this.parent();
            const next = parent.clone();
            const container = parent.parent();
            parent.find('input[type=hidden]').val(mediaAttachment.id);
            if(!parent.find('input[type=text]').val()){
                parent.find('input[type=text]').val(mediaAttachment.title);
            }
            parent.find('.file-action .aycpt-showfilemulti').attr('href', mediaAttachment.url);
            $this.addClass('hide');
            parent.find('.file-action').removeClass('hide');
            parent.find('.aycpt-addlinemulti').addClass('hide');
            parent.find('.aycpt-deletelinemulti').removeClass('hide');
            container.append(next);
            $this = next.find('.aycpt-addfilemulti');
        });
    });

    ayMediaframe.open();
});

// Delete file
$(document.body).on('click', '.aycpt-deletefilemulti', function (e) {
    e.preventDefault();
    const parent = $(this).parents('.file_mulitple');
    parent.find('input[type="hidden"]').val('');
    parent.find('.file-action').addClass('hide');
    parent.find('.file-action .aycpt-showfilemulti').attr('href', '');
    parent.find('.aycpt-addfilemulti').removeClass('hide');
});

