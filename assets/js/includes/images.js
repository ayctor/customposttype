// Add image
$(document.body).on('click', '.aycpt-addimage', function (e) {
    e.preventDefault();

    let ayMediaframe;
    const target = $(this).data('target');

    if (undefined !== ayMediaframe) {
        ayMediaframe.open();
        return;
    }

    ayMediaframe = wp.media.frames.ayMediaframe = wp.media({
        frame: 'post',
        multiple: false,
    });

    ayMediaframe.on( 'open', function() {
        $('.media-frame-title h1').text('Ajouter image : ');
        $('.media-button-insert').text('Sélectionner comme image ');
    });

    $('.media-button-insert').on('change', function(){
        $(this).text('Sélectionner comme image ');
    });

    ayMediaframe.on('insert', function () {
        const mediaAttachment = ayMediaframe.state().get('selection').first().toJSON();
        const parent = $('#' + target).parent();

        $('#' + target).val(mediaAttachment.id);
        parent.find('img').attr('src', mediaAttachment.url);
        parent.find('.aycpt-deleteimage').removeClass('hide');
        parent.find('.aycpt-addimage').toggleClass('hide');
    });

    ayMediaframe.open();
});

// Delete image

$(document.body).on('click', '.aycpt-deleteimage', function (e) {
    e.preventDefault();
    const target = $(this).data('target');
    const parent = $('#' + target).parent();
    $('#' + target).val('');
    parent.find('.aycpt-deleteimage').addClass('hide');
    parent.find('.aycpt-addimage').toggleClass('hide');
});
