let options = {
    // Strings and translations
    monthsFull: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
        'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun',
        'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
    weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],

    // Buttons
    today: 'Aujourd\'hui',
    clear: 'Effacer',
    close: 'Fermer',

    // Accessibility labels
    labelMonthNext: 'Mois suivant',
    labelMonthPrev: 'Mois précédant',
    labelMonthSelect: 'Sélectionner un mois',
    labelYearSelect: 'Sélectionner une année',

    // Formats
    format: 'dd/mm/yyyy',

    // First day of the week
    firstDay: 1,
};
$('[data-datepicker]').each(function () {
    const additionalOptions = $(this).data('datepicker');
    $.extend(options, additionalOptions);
    $(this).pickadate(options);
});
