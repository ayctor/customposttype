let options = {
    // Translations and clear button
    clear: 'Effacer',

    // Formats
    format: 'HH:i',
};
$('[data-timepicker]').each(function () {
    const additionalOptions = $(this).data('timepicker');
    $.extend(options, additionalOptions);
    $(this).pickatime(options);
});
