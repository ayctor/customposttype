<?php
/**
 * HtmlBuilder Class for editor
 *
 * @package ayctor\CustomPostType
 */
namespace CustomPostType;

/**
 * Class HtmlBuilder
 * @package CustomPostType
 */
class HtmlBuilder
{
    /**
     * Build Html for Info type
     *
     * @param $postId
     * @param $name
     * @param string $value
     * @return mixed|string
     */
    public function info($postId, $name, $value = '-')
    {
        if (is_string($value)) {
            return $value;
        } else if (is_callable($value)) {
            return $value($postId);
        }
    }

    /**
     * Build Html input
     *
     * @param $type
     * @param $name
     * @param $value
     * @param array $options
     * @return string
     */
    public function input($type, $name, $value, $options = [])
    {
        if (!isset($options['attributes']['name'])) {
            $options['attributes']['name'] = $name;
        }
        if (!isset($options['attributes']['value'])) {
            $options['attributes']['value'] = $value;
        }
        if (!isset($options['attributes']['type'])) {
            $options['attributes']['type'] = $type;
        }

        return '<input ' . $this->attributes($options['attributes']) . '>';
    }

    /**
     * Build Html for Text type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function text($postId, $name, $options = [])
    {
        $value = get_post_meta($postId, $name, true);
        return $this->input('text', $name, $value, $options);
    }

    /**
     * Build Html for Email type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function email($postId, $name, $options = [])
    {
        $value = get_post_meta($postId, $name, true);
        return $this->input('email', $name, $value, $options);
    }

    /**
     * Build Html for Tel type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function tel($postId, $name, $options = [])
    {
        $value = get_post_meta($postId, $name, true);
        return $this->input('tel', $name, $value, $options);
    }

    /**
     * Build Html for Date type (datepicker build with pickadate js)
     * http://amsul.ca/pickadate.js/date/
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function date($postId, $name, $options = [])
    {
        if (isset($options['attributes']['data-datepicker'])) {
            $options['attributes']['data-datepicker'] = json_encode($options['attributes']['data-datepicker']);
        } else {
            $options['attributes']['data-datepicker'] = '';
        }
        $value = get_post_meta($postId, $name, true);
        return $this->input('text', $name, $value, $options);
    }

    /**
     * Build Html for Time type (timepicker build with pickadate js)
     * http://amsul.ca/pickadate.js/time/
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function time($postId, $name, $options = [])
    {
        if (isset($options['attributes']['data-timepicker'])) {
            $options['attributes']['data-timepicker'] = json_encode($options['attributes']['data-timepicker']);
        } else {
            $options['attributes']['data-timepicker'] = '';
        }
        $value = get_post_meta($postId, $name, true);
        return $this->input('text', $name, $value, $options);
    }

    /**
     * Build Html for Number type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function number($postId, $name, $options = [])
    {
        $value = 0;
        if (get_post_meta($postId, $name, true)) {
            $value = get_post_meta($postId, $name, true);
        }
        return $this->input('number', $name, $value, $options);
    }

    /**
     * Build Html for Checkbox type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function checkbox($postId, $name, $options = [])
    {
        return $this->labelInput($postId, 'checkbox', $name, $options);
    }

    /**
     * Build Html for Radio type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function radio($postId, $name, $options = [])
    {
        return $this->labelInput($postId, 'radio', $name, $options);
    }

    /**
     * Label for input checkbox / radio
     *
     * @param $postId
     * @param $type
     * @param $name
     * @param $options
     * @return string
     */
    public function labelInput($postId, $type, $name, $options)
    {
        $content = '';
        $meta_values = get_post_meta($postId, $name, true);
        if (is_array($meta_values)) {
            $name = $name . '[]';
        }
        if (isset($options['options'])) {
            foreach ($options['options'] as $key => $value) {
                $checked = null;
                if (is_array($meta_values)) {
                    if (in_array($key, $meta_values)) {
                        $checked = 'checked';
                    }
                } elseif ($key == $meta_values) {
                    $checked = 'checked';
                }
                $inline = 'label-block';
                if (isset($options['inline']) && $options['inline'] !== false) {
                    $inline = 'label-inline';
                }
                $content .= '<label class="label-input ' . $inline . '">' . $this->input($type, $name, $key, ['attributes' => ['checked' => $checked]]) . $value . '</label>';
            }
        }
        return $content;
    }

    /**
     * Build Html for textarea type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function textarea($postId, $name, $options = [])
    {
        if (!isset($options['attributes']['name'])) {
            $options['attributes']['name'] = $name;
        }

        if (!isset($options['attributes']['rows'])) {
            $options['attributes']['rows'] = 5;
        }

        return '<textarea ' . $this->attributes($options['attributes']) . '>' . get_post_meta($postId, $name, true) . '</textarea>';
    }

    /**
     * Build Html for Editor type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function editor($postId, $name, $options = [])
    {
        if (!isset($options['textarea_rows'])) {
            $options['textarea_rows'] = 5;
        }
        ob_start();
        wp_editor(get_post_meta($postId, $name, true), $name, $options);
        $editor_content = ob_get_clean();
        return $editor_content;
    }

    /**
     * Build Html for File type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function file($postId, $name, $options = [])
    {
        $meta_value = get_post_meta($postId, $name, true);
        $options['attributes']['id'] = $name;
        if (!isset($options['attributes']['label'])) {
            $options['attributes']['label'] = 'Fichier';
        }
        $content = $this->input('hidden', $name, $meta_value, $options);
        $content .= '<span class="filename">';
        $hide = ' hide';
        if ($meta_value != '') {
            $hide = '';
        }
        $content .= '<a href="' . wp_get_attachment_url($meta_value) . '" target="_blank" class="' . $hide . '">' . get_the_title($meta_value) . '</a> ';
        $content .= '</span>';
        $content .= '<button class="button button-secondary aycpt-addfile aycpt-modifyfile' . $hide . '" data-target="' . $name . '" data-label="' . $options['attributes']['label'] . '">Modifier le fichier</button>';
        $content .= ' <button class="aycpt-deletefile button button-danger' . $hide . '" data-target="' . $name . '" ><span class="dashicons dashicons-trash"></span></button>';
        if ($meta_value == '') {
            $hide = '';
        } else {
            $hide = ' hide';
        }
        $content .= '<button class="button button-secondary aycpt-addfile' . $hide . '" data-target="' . $name . '" data-label="' . $options['attributes']['label'] . '">Ajouter un fichier</button>';
        return $content;
    }

    /**
     * Build Html for file multiple type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function file_multiple($postId, $name, $options = [])
    {
        $files = get_post_meta($postId, $name, true);
        $content = '<div id="' . $name . '">';
        if (!$files) {
            $files = ['ids' => [''], 'names' => ['']];
        }
        for ($i = 0; $i < count($files['ids']); $i++) {
            $content .= '<div class="file_mulitple">';
            $content .= $this->input('hidden', $name . "[ids][]", $files['ids'][$i]);
            $content .= $this->input('text', $name . "[names][]", $files['names'][$i], ['attributes' => ['placeholder' => 'Nom du fichier']]);
            if (isset($files['ids'][$i]) && !empty($files['ids'][$i])) {
                $spanClass = 'file-action';
                $buttonClass = 'button button-secondary aycpt-addfilemulti hide';
            } else {
                $spanClass = 'file-action hide';
                $buttonClass = 'button button-secondary aycpt-addfilemulti';
            }
            $content .= '<span class="' . $spanClass . '"><a class="aycpt-showfilemulti" href="' . wp_get_attachment_url($files['ids'][$i]) . '" target="_blank"><span class="dashicons dashicons-share-alt"></span></a> ';
            $content .= ' <a class="aycpt-deletefilemulti text-red">Supprimer le fichier</a></span>';
            $content .= '<button type="button" class="' . $buttonClass . '" data-label="' . $name . '" data-target="' . $name . '">Ajouter un fichier</button>';
            $hide = '';
            if ($i == 0) {
                $hide = ' hide';
            }
            $content .= ' <button type="button" class="aycpt-deletelinemulti button button-danger' . $hide . '" data-target="' . $name . '"><span class="dashicons dashicons-trash"></span></button>';
            if ($i == count($files['ids']) - 1) {
                $hide = '';
            }
            $content .= ' <button type="button" class="aycpt-addlinemulti button button-add' . $hide . '" data-label="' . $name . '" data-target="' . $name . '"><span class="dashicons dashicons-plus"></span></button>';
            $content .= '</div>';
        }
        $content .= '</div>';

        return $content;
    }

    /**
     * Build Html for Image type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function image($postId, $name, $options = [])
    {
        if (!isset($options['attributes']['width'])) {
            $options['attributes']['width'] = 150;
        }
        if (!isset($options['attributes']['height'])) {
            $options['attributes']['height'] = 150;
        }
        $content = '';
        $value = get_post_meta($postId, $name, true);
        if (!$value) {
            $hide = ' hide';
        } else {
            $hide = '';
        }
        $content .= '<a class="aycpt-addimage' . $hide . '" data-target="' . $name . '">';
        $content .= '<img src="' . wp_get_attachment_url(get_post_meta($postId, $name, true)) . '" alt="" width="' . $options['attributes']['width'] . '" height="' . $options['attributes']['height'] . '">';
        $content .= '</a>';
        $content .= ' <span class="dashicons dashicons-no-alt close aycpt-deleteimage' . $hide . '" data-target="' . $name . '"></span>';
        if ($value == '') {
            $hide = '';
        } else {
            $hide = ' hide';
        }
        $content .= '<button class="button-secondary aycpt-addimage' . $hide . '" data-target="' . $name . '">Ajouter une image</button>';
        $content .= $this->input('hidden', $name, get_post_meta($postId, $name, true), ['attributes' => ['id' => $name, 'name' => $name]]);
        return $content;
    }

    /**
     * Build Html for Gallery type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function gallery($postId, $name, $options = [])
    {
        $galeries = get_post_meta($postId, $name, true);
        if (is_string($galeries)) {
            $galeries = [];
        }
        if (!isset($options['attributes']['width'])) {
            $options['attributes']['width'] = 150;
        }
        if (!isset($options['attributes']['height'])) {
            $options['attributes']['height'] = 150;
        }

        $content = '<div class="galeries">';
        $content .= '<ul class="sortable">';
        $content .= '<li class="model hide ui-state-default">' . $this->input('hidden', '', '') .'<img src="" alt="" width="' . $options['attributes']['width'] . '" height="' . $options['attributes']['height'] . '"><span class="dashicons dashicons-no-alt close"></span></li>';
        foreach ($galeries as $gallery) {
            $content .= '<li class="ui-state-default">' . $this->input('hidden', $name . '[]', $gallery);
            $image = wp_get_attachment_image($gallery, array($options['attributes']['width'], $options['attributes']['height']));
            if (empty($image)) {
                $image = '<img src="' . get_template_directory_uri() . '/vendor/ayctor/cpt/assets/interface.png" width="' . $options['attributes']['width'] . '" height="' . $options['attributes']['height'] . '">';
            }
            $content .= $image . '<span class="dashicons dashicons-no-alt close"></span>';
            $content .= '</li>';
        }
        $content .= '</ul>';
        $content .= '<button class="button-primary aycpt-addgallery" data-target="' . $name . '">Ajouter à la galerie</button>';
        $content .= '</div>';
        return $content;
    }

    /**
     * Build Html for Select type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function select($postId, $name, $options = [])
    {
        $html = [];
        if (isset($options['options'])) {
            foreach ($options['options'] as $value => $display) {
                $selected = false;
                $disabled = [];
                if (is_array(get_post_meta($postId, $name, true))) {
                    if (in_array($value, get_post_meta($postId, $name, true))) {
                        $disabled = ['disabled' => 'disabled'];
                    }
                }
                if (empty(get_post_meta($postId, $name, true)) and $value === '') {
                    $selected = 'selected';
                } else if (get_post_meta($postId, $name, true) == $value and !empty(get_post_meta($postId, $name, true))) {
                    $selected = 'selected';
                }
                $attrs = array_merge(['value' => $value, $selected ? 'selected' : ''], $disabled);
                $html[] = '<option' . $this->attributes($attrs) . '>' . htmlentities($display) . '</option>';
            }
        }
        if (!isset($options['attributes'])) {
            $options['attributes'] = [];
        }
        if (!isset($options['attributes']['name'])) {
            $options['attributes']['name'] = $name;
        }
        $options['attributes']['style'] = 'width: 80%;';
        $list = implode('', $html);
        return ("<select{$this->attributes($options['attributes'])}>{$list}</select>");
    }

    /**
     * Build Html for Select multiple type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function select_multiple($postId, $name, $options = [])
    {
        $content = '<div class="multiple-select">';
        $content .= $this->select($postId, $name, $options);
        $content .= ' <button type="button" class="button button-add multiple-select-add" data-target="' . $name . '"><span class="dashicons dashicons-plus"></span></button>';
        $content .= '<ul>';
        $content .= '<li class="model hide"><input type="hidden" name="' . $name . '[]' . '"><span class="text"></span> <button type="button" class="button button-danger multiple-select-delete"><span class="dashicons dashicons-trash"></span></button></li>';
        $values = get_post_meta($postId, $name, true);
        if (is_array($values)) {
            foreach ($values as $value) {
                $content .= '<li>';
                $content .= $this->input('hidden', $name . '[]', $value, null);
                $content .= $options['options'][$value];
                $content .= ' <button type="button" class="button button-danger multiple-select-delete"><span class="dashicons dashicons-trash"></span></button>';
                $content .= '</li>';
            }
        }
        $content .= '</ul>';
        $content .= '</div>';
        return $content;
    }

    /**
     * Build Html for Multiple text type
     *
     * @param $postId
     * @param $name
     * @param array $options
     * @return string
     */
    public function text_multiple($postId, $name, $options = [])
    {
        $content = '';
        $meta_value = get_post_meta($postId, $name, true);

        if (!is_array($meta_value)) {
            $meta_value = array($meta_value);
        }
        if (isset($options['number']) && is_int($options['number'])) {
            for ($i = 0; $i < $options['number']; $i++) {
                $value = '';
                if (isset($meta_value[$i])) {
                    $value = $meta_value[$i];
                }
                $content .= $i + 1 . $this->input('text', $name . '[]', $value, $options) . '<br>';
            }
        } else {
            $content .= '<div class="text-multiple-container">';
            foreach ($meta_value as $key => $value) {
                $content .= '<div class="text-multiple">';
                $content .= $this->input('text', $name . '[]', $value, $options);
                $hide = '';
                if ($key == 0) {
                    $hide = ' hide';
                }
                $content .= ' <button type="button" class="button button-danger aycpt-deleteline' .  $hide . '"><span class="dashicons dashicons-trash"></span></button>';
                if ($key == count($meta_value) - 1) {
                    $hide = '';
                }
                $content .= ' <button type="button" class="button button-add aycpt-addline' .  $hide . '" data-target="' . $name . '"><span class="dashicons dashicons-plus"></span></button>';
                $content .= '</div>';
            }
            $content .= '</div>';
        }
        return $content;
    }

    /**
     * Build Html attributes
     *
     * @param $attributes
     * @return string
     */
    public function attributes($attributes)
    {
        $html = [];

        foreach ($attributes as $key => $value) {
            $element = '';

            if (!is_null($value) && !is_array($value)) {
                $element = $key . '="' . htmlentities($value) . '"';
            }

            if ($key == $value) {
                $element = $value;
            }

            if (!is_null($element)) {
                $html[] = $element;
            }
        }

        return $html ? ' ' . implode(' ', $html) : '';
    }
}
