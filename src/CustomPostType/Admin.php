<?php
/**
 * Admin actions for CPT package
 *
 * @package ayctor\CustomPostType
 * @author Erwan Guillon <erwan@ayctor.com>
 */
namespace CustomPostType;

/**
 * Admin actions for CPT package
 */
class Admin
{
    /**
     * Actions and filters
     */
    public function __construct()
    {
        add_action('admin_enqueue_scripts', [$this, 'adminScripts']);
        add_action('admin_print_styles', [$this, 'adminStyles'], 11);
        add_filter('upload_mimes', [$this, 'mimeTypes']);
    }

    /**
     * Enqueue admin scripts
     */
    public function adminScripts()
    {
        global $post;
        if (isset($post->ID)) {
            wp_enqueue_media(['post' => $post->ID]);
        }
        wp_enqueue_script('aycptjs', get_template_directory_uri() . '/vendor/ayctor/cpt/assets/cpt.js', [], filemtime(dirname(__FILE__) . '/../../assets/cpt.js'), true);
        $urls = array(
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'templateUrl' => get_template_directory_uri(),
        );
        wp_localize_script('aycptjs', 'urls', $urls);
    }

    /**
     * Anqueue admin styles
     */
    public function adminStyles()
    {
        wp_enqueue_style('aycptcss', get_template_directory_uri() . '/vendor/ayctor/cpt/assets/cpt.css');
    }

    /**
    * Allow svg to be uploaded in media uploader
    * @param  Array $mimes Mimes types
    * @return Array        Updated mimes types
    */
    public function mimeTypes($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
}
