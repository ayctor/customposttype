<?php
/**
 * Model Class to extend
 *
 * @package ayctor\CustomPostType
 */
namespace CustomPostType;

use Inflect\Inflect;
use CustomPostType\HtmlBuilder;

/**
 * Model Class to extend
 * @todo add getter function
 */
class Model extends Editor
{
    /**
     * Internal name for the custom post type
     * @var string
     */
    protected $internal_name = '';

    /**
     * Main display name for the custom post type. It tries to generate all the
     * other labels from this one.
     * @var string
     */
    protected $name = '';

    /**
     * List of labels for the custom post type.
     * @see https://codex.wordpress.org/Function_Reference/register_post_type
     * for more information on labels
     * @var string[]
     */
    protected $labels = [];

    /**
     * Custom post type arguments for register_post_type function
     * @see https://codex.wordpress.org/Function_Reference/register_post_type
     * for more informations on arguments
     * @var array
     */
    protected $cpt_args = [];

    /**
     * Contains Array of Custom Taxonomies params
     * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
     * for more informations
     * @var array
     */
    protected $taxonomies = [];

    /**
     * HtmlBuilder Class
     *
     * @var \CustomPostType\HtmlBuilder
     */
    protected $htmlBuilder;

    /**
     * Create a new instance
     */
    public function __construct()
    {
        $this->htmlBuilder = new HtmlBuilder();

        $this->cpt_args['labels'] = $this->generateLabels();
        $this->cpt_args['register_meta_box_cb'] = [$this, 'registerMetaBoxes'];

        // Post actions
        add_action('init', [$this, 'init']);
        add_action('save_post', [$this, 'metaBoxSave']);

        // Init editor
        add_action('current_screen', function ($current_screen) {
            if ($current_screen->post_type == $this->internal_name) {
                $this->adminEditor();

                // Filters
                add_action('restrict_manage_posts', [$this, 'customFilters']);
                add_filter('parse_query', [$this, 'actionFilters']);
            }
        });

        // Columns
        add_filter('manage_edit-' . $this->internal_name . '_columns', [$this, 'customColumns']);
        add_filter('manage_edit-' . $this->internal_name . '_sortable_columns', [$this, 'customColumnsSort']);
        add_action('manage_posts_custom_column', [$this, 'customColumnsShow']);
        add_action('manage_pages_custom_column', [$this, 'customColumnsShow']);
    }

    /**
     * Init the register_post_type function
     * @return void
     */
    public function init()
    {
        register_post_type($this->internal_name, $this->cpt_args);

        if (!empty($this->taxonomies)) {
            foreach ($this->taxonomies as $taxo_id => $taxo_params) {
                register_taxonomy($taxo_id, $this->internal_name, $taxo_params);
            }
        }
    }

    /**
     * Load all data to CPT editor page
     */
    public function adminEditor()
    {
    }

    /**
     * Register the meta boxes
     */
    public function registerMetaBoxes()
    {
        foreach ($this->meta_boxes as $meta_box) {
            add_meta_box($meta_box['id'], $meta_box['title'], array($this, 'metaBoxHtml'), $this->internal_name, $meta_box['context'], $meta_box['priority'], $meta_box['args']);
        }
    }

    /**
     * Display the meta box html
     * @param  WP_Post $post Current post
     * @param  array $args Additional arguments
     */
    public function metaBoxHtml($post, $args)
    {
        wp_nonce_field(plugin_basename(__FILE__), $this->internal_name);
        $class = 'form-group';
        if (isset($args['args']) && isset($args['args']['class'])) {
            $class .= ' ' . $args['args']['class'];
        }
        if (isset($this->meta_fields[$args['id']]) && !empty($this->meta_fields[$args['id']])) {
            foreach ($this->meta_fields[$args['id']] as $field) {
                if (isset($this->meta_labels[$args['id']][$field['name']])) {
                    $label = $this->meta_labels[$args['id']][$field['name']];
                } else {
                    $label = false;
                }
                $field = $this->htmlBuilder->{$field['type']}($post->ID, $field['name'], $field['options']);
                ?>
                <div class="<?php echo $class; ?>">
                    <?php if ($label) : ?>
                        <div class="label"><?php echo $label ?></div>
                    <?php endif; ?>
                    <div class="input"><?php echo $field ?></div>
                </div>
                <?php
            }
        }
    }

    /**
     * When the save action is triggered, we save the meta data
     * @param integer $post_id Current post id
     * @return boolean|null
     */
    public function metaBoxSave($post_id)
    {
        if (!is_user_logged_in()) {
            return;
        }

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!isset($_POST[$this->internal_name]) or !wp_verify_nonce($_POST[$this->internal_name], plugin_basename(__FILE__))) {
            return;
        }

        if ($this->internal_name == $_POST['post_type']) {
            if (!current_user_can('edit_post', $post_id)) {
                return;
            }
        } else {
            return;
        }

        foreach ($this->meta_fields as $k => $meta_box) {
            foreach ($meta_box as $field) {
                if (isset($_POST[$field['name']]) && is_array($_POST[$field['name']])) {
                    if (isset($_POST[$field['name']]['ids'])) {
                        $arrayField = array_filter($_POST[$field['name']], function ($value, $key) {
                            if ($key == 'ids' && $value == '') {
                                return false;
                            }
                            return true;
                        }, ARRAY_FILTER_USE_BOTH);
                    } else {
                        $arrayField = array_filter($_POST[$field['name']]);
                    }

                    if (!empty($arrayField)) {
                        update_post_meta($post_id, $field['name'], $arrayField);
                    } else {
                        delete_post_meta($post_id, $field['name']);
                    }
                } elseif (isset($_POST[$field['name']]) && !empty($_POST[$field['name']])) {
                    update_post_meta($post_id, $field['name'], $_POST[$field['name']]);
                } else {
                    delete_post_meta($post_id, $field['name']);
                }
            }
        }
        return true;
    }

    /**
     * Change the columns when listings the custom post
     * @param array $columns Contains default columns to display
     * @return array Columns to display
     */
    public function customColumns($columns)
    {
        if (!empty($this->columns)) {
            return $this->columns;
        }
        return $columns;
    }

    /**
     * Define the sortable columns
     * @param array $columns Array of string for column's name
     * @return array Formatted array for sorting columns
     */
    public function customColumnsSort($columns)
    {
        if (!empty($this->sortable_columns)) {
            return $this->sortable_columns;
        }
        return $columns;
    }

    /**
     * Define the content for custom columns
     * @param string $name Name for current column to display
     */
    public function customColumnsShow($name)
    {
        global $post;
        if ($post->post_type == $this->internal_name) {
            foreach ($this->columns_show as $column) {
                if ($name == $column['id']) {
                    if ($column['type'] == 'meta') {
                        echo get_post_meta($post->ID, $column['id'], true);
                    } else if ($column['type'] == 'term') {
                        $terms = get_the_term_list($post->ID, $column['id'], '', ',', '');
                        if (is_string($terms)) {
                            echo $terms;
                        } else {
                            echo '-';
                        }
                    } else if ($column['type'] == 'thumbnail') {
                        echo get_the_post_thumbnail($post->ID, 'thumbnail');
                    } else if ($column['type'] == 'custom') {
                        if (is_callable($column['custom'])) {
                            $column['custom']();
                        } else if (is_string($column['custom'])) {
                            echo $column['custom'];
                        } else {
                            echo '-';
                        }
                    } else {
                        echo '-';
                    }
                }
            }
        }
    }

    /**
     * Selector for custom filter
     */
    public function customFilters()
    {
        if (isset($_GET['post_type']) && $_GET['post_type'] == $this->internal_name) {
            foreach ($this->filters as $filter) {
                ?>
                <select name="<?php echo $filter['name']; ?>" id="<?php echo $filter['name']; ?>">
                    <?php foreach ($filter['values'] as $key => $value) : ?>
                        <?php $selected = (isset($_GET[$filter['name']]) and $_GET[$filter['name']] == $key) ? 'selected="select"' : ''; ?>
                        <option value="<?php echo $key; ?>" <?php echo $selected ?>>
                            <?php echo $value; ?>
                        </option>
                    <?php endforeach ?>
                </select>
                <?php
            }
        }
    }

    /**
     * Filters actions for program
     * @param object $query Query to display data
     */
    public function actionFilters($query)
    {
        if (is_admin() and isset($query->query['post_type']) and $query->query['post_type'] == $this->internal_name) {
            $qv = &$query->query_vars;
            $qv['meta_query'] = [];
            foreach ($this->filters as $filter) {
                if (!$filter['action']) {
                    if (isset($_GET[$filter['name']]) and $_GET[$filter['name']] != '') {
                        $qv['meta_query'][] = ['key' => $filter['name'], 'value' => urldecode($_GET[$filter['name']])];
                    }
                } else {
                    if (isset($_GET[$filter['name']]) and $_GET[$filter['name']] != '') {
                        foreach ($filter['action'] as $action) {
                            $qv['meta_query'][] = $action;
                        }
                    }
                }
            }
        }
    }

    /**
     * Generate Labels for CPT
     * @return Array Labels
     */
    protected function generateLabels()
    {
        if (isset($this->cpt_args['labels']) && !empty($this->cpt_args['labels'])) {
            return $this->cpt_args['labels'];
        }

        if (!empty($this->labels)) {
            return $this->labels;
        }

        if (!empty($this->name)) {
            $label = $this->name;
        } else {
            $label = $this->internal_name;
        }

        $singular = Inflect::singularize($label);
        $plural = Inflect::pluralize($label);
        $labels = [
            'add_new_item' => 'Ajouter ' . $singular,
            'all_items' => $plural,
            'name' => $plural,
            'archives' => 'Archives des ' . $plural,
            'attributes' => 'Attributs des ' . $plural,
            'edit_item' => 'Editer ' . $singular,
            'filter_items_list' => 'Filtrer la liste des ' . $plural,
            'insert_into_item' => 'Insérer dans ' . $singular,
            'items_list' => 'Liste des ' . $plural,
            'items_list_navigation' => 'Navigation de la liste des ' . $plural,
            'name_admin_bar' => $singular,
            'new_item' => '',
            'not_found' => $singular . ' introuvable',
            'not_found_in_trash' => $singular . ' introuvable dans la corbeille',
            'search_items' => 'Chercher ' . $plural,
            'singular_name' => $singular,
            'view_item' => 'Voir ' . $singular,
            'view_items' => 'Voir ' . $plural,
            'uploaded_to_this_item' => 'Uploader dans ' . $singular,
        ];
        return $labels;
    }
}
