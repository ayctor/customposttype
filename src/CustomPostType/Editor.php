<?php
/**
 * Editor Class for model
 *
 * @package ayctor\CustomPostType
 */
namespace CustomPostType;

/**
 * Editor Class for model
 */
class Editor
{
    /**
     * Contains meta boxes informations when using addMetaBoxes function
     * @var array
     */
    protected $meta_boxes = [];

    /**
     * Contains meta fields information
     * @var array
     */
    protected $meta_fields = [];

    /**
     * Contains labels for meta fields
     * @var array
     */
    protected $meta_labels = [];

    /**
     * Contains Custom columns
     * @var array
     */
    protected $columns = [];

    /**
     * Contains sortable Columns
     * @var array
     */
    protected $sortable_columns = [];

    /**
     * Contains Columns display
     * @var array
     */
    protected $columns_show = [];

    /**
     * Contains Filters
     * @var array
     */
    protected $filters = [];


    /**
     * Add a metabox to the CustomPostType admin editor
     * @param string $id Id of the metabox, must be unique
     * @param string $title Title of the metabox
     * @param string $context Context : normal, side or advanced
     * @param string $priority Priority : default, high or low
     */
    protected function metaBox($id, $title, $context = 'normal', $priority = 'default', $args = null)
    {
        $this->meta_boxes[] = compact('id', 'title', 'context', 'priority', 'args');
    }

    /**
     * Add a label for a field
     * @param  string $label Label for admin editing
     * @param  string $for name of meta field
     * @param  string $meta_box_id Custom metaboxe's id
     * @return @void
     */
    protected function label($label, $for, $meta_box_id)
    {
        if (!isset($this->meta_labels[$meta_box_id])) {
            $this->meta_labels[$meta_box_id] = [];
        }
        $this->meta_labels[$meta_box_id][$for] = $label;
    }

    /**
     * Add a field to a custom metabox
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param string $type Field type
     * @param array $options List of options
     */
    protected function field($name, $meta_box_id, $type, $options = [])
    {
        if (!isset($this->meta_fields[$meta_box_id])) {
            $this->meta_fields[$meta_box_id] = [];
        }
        $this->meta_fields[$meta_box_id][] = compact('name', 'type', 'options');
    }

    /**
     * Add field of type info
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function info($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'info', $options);
    }

    /**
     * Add field of type text
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function text($name, $meta_box_id, $options = [])
    {
        $type = 'text';
        if (isset($options['multiple']) and $options['multiple'] == true) {
            $type = 'text_multiple';
        }
        $this->field($name, $meta_box_id, $type, $options);
    }

    /**
     * Add field of type number
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function number($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'number', $options);
    }

    /**
     * Add field of type date
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function date($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'date', $options);
    }

    /**
     * Add field of type time
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function time($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'time', $options);
    }

    /**
     * Add field of type email
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function email($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'email', $options);
    }

    /**
     * Add field of type tel
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function tel($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'tel', $options);
    }

    /**
     * Add field of type file
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function file($name, $meta_box_id, $options = [])
    {
        $type = 'file';
        if (isset($options['multiple']) and $options['multiple'] == true) {
            $type = 'file_multiple';
        }
        $this->field($name, $meta_box_id, $type, $options);
    }

    /**
     * Add field of type textarea
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function textarea($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'textarea', $options);
    }

    /**
     * Add field of type select
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function select($name, $meta_box_id, $options = [])
    {
        $type = 'select';
        if (isset($options['multiple']) and $options['multiple'] == true) {
            $type = 'select_multiple';
        }
        $this->field($name, $meta_box_id, $type, $options);
    }

    /**
     * Add field of type cpt
     * @param $name
     * @param $meta_box_id
     * @param array $options
     */
    protected function cpt($name, $meta_box_id, $options = [])
    {
        if (!isset($options['args'])) {
            $options['args'] = [
                'post_type' => 'post'
            ];
        }
        $posts = get_posts($options['args']);
        $options['options'] = ['' => ''];
        foreach ($posts as $post) {
            $options['options'][$post->ID] = $post->post_title;
        }
        $this->select($name, $meta_box_id, $options);
    }

    /**
     * Add field of type radio
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function radio($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'radio', $options);
    }

    /**
     * Add field of type checkbox
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function checkbox($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'checkbox', $options);
    }

    /**
     * Add field of type editor
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function editor($name, $meta_box_id, $options = [])
    {
        $this->field($name, $meta_box_id, 'editor', $options);
    }

    /**
     * Add field of type image
     * @param string $name Field's name. It will be the meta key.
     * @param string $meta_box_id Custom metaboxe's id
     * @param array $options List of options
     */
    protected function image($name, $meta_box_id, $options = [])
    {
        $type = 'image';
        if (isset($options['gallery']) and $options['gallery'] == true) {
            $type = 'gallery';
        }
        $this->field($name, $meta_box_id, $type, $options);
    }

    /**
     * Add a custom column for the listing page
     * @param string $id Column's id, must be unique
     * @param string $label Column's name
     * @param boolean $sort Is the column sortable
     * @param boolean $type The type of the content to display
     * @param mixed $custom Custom value to display
     */
    protected function column($id, $label, $sort = false, $type = false, $custom = '-')
    {
        $this->columns[$id] = __($label);
        if ($sort) {
            $this->sortable_columns[$id] = $id;
        }
        if ($type) {
            $this->columns_show[] = compact('id', 'type', 'custom');
        }
    }

    /**
     * Add a filter
     * @param $name
     * @param $values
     * @param bool $action
     */
    protected function filter($name, $values, $action = false)
    {
        $this->filters[] = compact('name', 'values', 'action');
    }
}
